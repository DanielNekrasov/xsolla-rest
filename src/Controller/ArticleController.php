<?php

namespace App\Controller;

use App\Entity\Article;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class ArticleController extends AbstractController {
    /**
     * @Route("/", name="article_list")
     * @Method({"GET"})
    */
    public function index() {
        $articles = $this->getDoctrine()->getRepository(Article::class)->findAll();
       return $this->render("articles/index.html.twig", array(
            'articles' => $articles
       ));
    }

    /**
     * @Route("/article/save")
     */
    public function save() {
        $entityManager = $this->getDoctrine()->getManager();

        $article = new Article();
        $article->setTitle('Article 1');
        $article->setBody('This is the body fot Article 1');

        $entityManager->persist($article);
        $entityManager->flush();

        return new Response('Saved an article'. $article->getId());
    }

    /**
     * Route("/article/new")
     * Method({"POST"})
     */
    public function new(Request $request) {
        $article = new Article();
//        $article = getData();

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($article);
        $entityManager->flush();

        return "";
    }

    /**
     * @Route("/articles/delete/{id}")
     * @Method({"DELETE"})
     */
    public function delete(Request $request, $id)
    {
        $article = $this->getDoctrine()->getRepository(Article::class)->find($id);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($article);
        $entityManager->flush();

        $response = new Response();
        $response->send();
    }
}